﻿
using QuickPool;
using UnityEngine;

namespace SpaceExplorer.Map.Tiles
{
    /// <summary>
    /// Класс Planet
    /// Реализация самой планеты. В конструкторе префаб не спаунится
    /// </summary>
    public class Planet : IRatingTile
    {
        readonly Vector2Int position;
        public Vector2Int Position
        {
            get
            {
                return position;
            }
        }

        GameObject prefab;
        const int skinIdMax = 13;

        readonly int rating;

        public Planet(Vector2Int _position, int _rating)
        {
            position = _position;
            rating = _rating;
        }

        public int GetRating
        {
            get
            {
                return rating;
            }
        }

        /// <summary>
        /// Спауним префаб
        /// </summary>
        public void ShowPrefab()
        {
            if (prefab == null)
            {
                //выбираем спрайт планеты из 14
                int skinId = rating % skinIdMax;
                prefab = PoolsManager.Spawn("Planet", new Vector3(position.x, position.y, 0), Quaternion.identity);
                prefab.transform.SetParent(GameObject.FindGameObjectWithTag("MapLayer").transform);//инкапсулировать
                ObjectsWithText planetInterface = prefab.GetComponent<ObjectsWithText>();
                planetInterface.ChangeSprite(skinId);
                planetInterface.TextLine = rating.ToString();
            }
        }

        public void DestroyPrefab()
        {
            if (prefab != null)
            {
                prefab.Despawn();
                prefab = null;
            }
        }
    }
}