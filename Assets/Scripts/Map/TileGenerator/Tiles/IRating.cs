﻿namespace SpaceExplorer.Map.Tiles
{
    public interface IRating
    {
        int GetRating
        {
            get;
        }
    }
}