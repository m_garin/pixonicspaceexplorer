using UnityEngine;

namespace SpaceExplorer.Map.Tiles
{
    public interface ITile
    {
        Vector2Int Position
        {
            get;
        }

        void ShowPrefab();

        void DestroyPrefab();
    }
}
