﻿using UnityEngine;

namespace SpaceExplorer.Map.Tiles.Repository
{
    public interface ITilesRepository
    {
        /// <summary>
        /// Берем из хранилища точку
        /// </summary>
        IRatingTile GetTile(Vector2Int _position);
    }
}