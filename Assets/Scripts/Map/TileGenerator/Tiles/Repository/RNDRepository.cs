﻿using SpaceExplorer.RNG;
using UnityEngine;

namespace SpaceExplorer.Map.Tiles.Repository
{
    /// <summary>
    /// Класс RNDRepository
    /// Генерирует тайлы с помощью хешфункций
    /// </summary>
    public class RNDRepository : ITilesRepository
    {
        enum TileTypeChance { Planet = 30 };
        IHashFunction findingProbabilityRND;
        IHashFunction tileRatingRND;
        const int ratingMax = 10000;

        public RNDRepository()
        {
            findingProbabilityRND = new FasterHash();
            tileRatingRND = new PerlinNoise();
        }

        public IRatingTile GetTile(Vector2Int _position)
        {
            if (findingProbabilityRND.Range(_position, 100) >= (int)TileTypeChance.Planet)
            {
                return new Planet(_position, tileRatingRND.Range(_position, ratingMax));
            }
            return null;
        }
    }
}