using UnityEngine;
using System.Collections.Generic;
using SpaceExplorer.Map.Tiles.Display;
using SpaceExplorer.Map.Tiles.Repository;

namespace SpaceExplorer.Map.Tiles
{
    /// <summary>
    /// Класс TileGenerator
    /// Получает тайлы из репозитория и передает дальше для вывода
    /// </summary>
    public class TileGenerator
    {
        readonly Dictionary<string, ITile> topTiles;
        readonly ITilesRepository tilesRepository;
        IDisplayMode<IRatingTile> displayMode; //тут производится отбор
        public IDisplayMode<IRatingTile> SetTilesViewer
        {
            set
            {
                displayMode = value;
            }
        }

        Dictionary<string, IRatingTile> TopTiles { get; set; }

        public TileGenerator(ITilesRepository _tilesRepository, IDisplayMode<IRatingTile> _tilesViewer)
        {
            topTiles = new Dictionary<string, ITile>(); //можно переделать на пробрасывание
            tilesRepository = _tilesRepository;
            displayMode = _tilesViewer;
        }

        public void Generate(Vector2Int _position)
        {
            IRatingTile tile = tilesRepository.GetTile(_position);
            if (tile != null)
                displayMode.CheckIn(tile);
        }

        public void Remove(Vector2Int _position)
        {
            displayMode.CheckOut(_position);
        }
    }
}
