
namespace SpaceExplorer.MathFunc
{
    /// <summary>
    /// Класс MyMathf
    /// Более быстрая реализация либы Math
    /// </summary>
    public class MyMathf : IMathf
    {
        public int Abs(int _value)
        {
            return (_value ^ (_value >> 31)) - (_value >> 31);
        }
    }
}
