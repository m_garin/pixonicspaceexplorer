using UnityEngine;

namespace SpaceExplorer.Map.Tiles.Display
{
    public interface IDisplayMode<TileType>
    {
        void CheckIn(TileType _tile);
        void CheckOut(Vector2Int _position);

        void ShowAll();
        void DestroyAll();
    }
}
