﻿using SpaceExplorer.Map.Tiles.Repository;
using SpaceExplorer.MathFunc;
using SpaceExplorer.Ship.Boundary;
using UnityEngine;

namespace SpaceExplorer.Map.Tiles.Display.Top
{
    /// <summary>
    /// Класс для хранения лучших (ближайших) тайлов чанка
    /// </summary>
    public class RatingChunk
    {
        public Vector2Int ChunkPosition { get; private set; }
        /// <summary>
        /// Координата ближайшего к игроку по рангу тайла в чанке
        /// </summary>
        public IRatingTile BestTile { get; private set; }
        /// <summary>
        /// Флаг необходимости пересчета чанка для поиска лучшего тайла
        /// </summary>
        public int TilesAlreadyAdded { get; private set; } //не будем хранить все тайлы, только кол-во добавленных (проверенных)

        public bool NeedRecalculate { get; private set; }

        IRating player;
        readonly int chunkSize;
        readonly IBoundaryController boundaryController;
        readonly ITilesRepository tilesRepository;
        readonly IMathf mathf;

        public RatingChunk(int _chunkSize, IRating _player, IRatingTile _tile, Vector2Int _chunkPosition, IBoundaryController _boundaryController, ITilesRepository _tilesRepository)
        {
            chunkSize = _chunkSize;
            boundaryController = _boundaryController;
            ChunkPosition = _chunkPosition;
            player = _player;
            BestTile = _tile;
            TilesAlreadyAdded = 1;
            tilesRepository = _tilesRepository;
            mathf = new MyMathf();
        }

        public void AddTile(IRatingTile _tile)
        {
            TilesAlreadyAdded++;
            if (BestTile == null)
                BestTile = _tile;
            else if (mathf.Abs(player.GetRating - _tile.GetRating) < mathf.Abs(player.GetRating - BestTile.GetRating))
                BestTile = _tile; //ура, у нас новый лучший
        }

        public void RemoveTile(Vector2Int _tilePosition)
        {
            TilesAlreadyAdded--;
            // если координаты лучшего тайла совпадаю с координатами текущего (скрываемого за область видимости) тайла, то отмечаем 
            // текущий чанк, как чанк для пересчета
            if (BestTile.Position.x == _tilePosition.x && BestTile.Position.y == _tilePosition.y)
            {
                NeedRecalculate = true;
            }
        }

        //пересчитываем чанк
        public void ChunkRecalculate()
        {
            TilesAlreadyAdded = 0;
            NeedRecalculate = false;
            int xOffset = ChunkPosition.x;
            int yOffset = ChunkPosition.y;
            BestTile = null;
            for (int i = 0; i < chunkSize; i++)
            {
                for (int j = 0; j < chunkSize; j++)
                {
                    int x = xOffset + i;
                    int y = yOffset + j;
                    // если точка находится в границах зоны видимости
                    RectInt mapBoundary = boundaryController.Boundary;
                    if (mapBoundary.xMin <= x && x <= mapBoundary.xMax && mapBoundary.yMin <= y && y <= mapBoundary.yMax)
                    {
                        // улучшаем результат
                        IRatingTile newTile = tilesRepository.GetTile(new Vector2Int(x, y));
                        if (newTile != null)
                            if (BestTile == null)
                            {
                                BestTile = newTile;
                            }
                            else
                            {
                                if (mathf.Abs(player.GetRating - newTile.GetRating) < mathf.Abs(player.GetRating - BestTile.GetRating))
                                {
                                    BestTile = newTile;
                                }
                                TilesAlreadyAdded++;
                            }
                    }
                }
            }
        }
    }
}
