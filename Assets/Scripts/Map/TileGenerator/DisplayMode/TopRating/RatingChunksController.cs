﻿using System.Collections.Generic;
using SpaceExplorer.Map.Tiles.Repository;
using SpaceExplorer.Ship.Boundary;
using UnityEngine;

namespace SpaceExplorer.Map.Tiles.Display.Top
{
    public class RatingChunksController
    {
        Dictionary<Vector2Int, RatingChunk> ratingChunks;
        IRating player;
        readonly IBoundaryController boundaryController;
        readonly ITilesRepository tilesRepository;
        /// <summary>
        /// Размер одного чанка (квадрат)
        /// </summary>
        int chunkSize = 25;

        public Dictionary<Vector2Int, RatingChunk> GetChunks
        {
            get
            {

                return ratingChunks;
            }
        }

        public bool ChunksReady(int _needCount)
        {
            int count = 0;
            foreach (RatingChunk chunk in ratingChunks.Values)
            {
                if (!chunk.NeedRecalculate)
                {
                    count++;
                    if (count >= _needCount)
                        return true;
                }
            }
            return false;
        }

        public RatingChunksController(IRating _player, IBoundaryController _boundaryController, ITilesRepository _tilesRepository)
        {
            boundaryController = _boundaryController;
            player = _player;
            ratingChunks = new Dictionary<Vector2Int, RatingChunk>();
            tilesRepository = _tilesRepository;
        }

        public void AddTile(IRatingTile _tile)
        {
            // вычисляем номер чанка
            Vector2Int chunkPosition = ChunkNumber(_tile.Position);
            RatingChunk chunk;
            //проверяем есть ли в списке уже такой чанк
            if (ratingChunks.TryGetValue(chunkPosition, out chunk))
            {
                chunk.AddTile(_tile); //добавляем в чанк
            }
            else
            {
                ratingChunks.Add(chunkPosition, new RatingChunk(chunkSize, player, _tile, chunkPosition, boundaryController, tilesRepository)); //создаем новый и добавляем его в список
            }
        }

        public void CheckOutTile(Vector2Int _tilePosition)
        {
            // вычисляем номер чанка
            Vector2Int key = ChunkNumber(_tilePosition);
            RatingChunk chunk;
            //проверяем есть ли в списке уже такой чанк
            if (ratingChunks.TryGetValue(key, out chunk))
            {
                if (chunk.TilesAlreadyAdded <= 1) //если это последний тайл в чанке, то удалим чанк
                {
                    ratingChunks.Remove(key);
                }
                else
                {
                    chunk.RemoveTile(_tilePosition); //удалим тайл из чанка
                }
            }
        }

        /// <summary>
        /// Метод вычисления "номера" чанка из координат
        /// </summary>
        Vector2Int ChunkNumber(Vector2Int _position)
        {
            _position.x = (_position.x < 0) ? chunkSize * (_position.x / chunkSize - 1) : chunkSize * (_position.x / chunkSize);
            _position.y = (_position.y < 0) ? chunkSize * (_position.y / chunkSize - 1) : chunkSize * (_position.y / chunkSize);
            return _position;
        }
    }
}

