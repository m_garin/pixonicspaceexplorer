using SpaceExplorer.Map.Tiles.Repository;
using SpaceExplorer.Ship.Boundary;
using UnityEngine;

namespace SpaceExplorer.Map.Tiles.Display.Top
{
    //для отображения только ближайших по рейтингу тайлов
    public class TopDisplay : IDisplayMode<IRatingTile>
    {
        /// <summary>
        /// Количество ближайщих тайлов
        /// </summary>
        const int capacity = 20;
        readonly TopTiles topTiles;
        readonly RatingChunksController ratingChunksController;
        readonly IBoundaryController boundaryController;
        readonly ITilesRepository tilesRepository;

        public TopDisplay(IRating _player, IBoundaryController _boundaryController, ITilesRepository _tilesRepository)
        {
            boundaryController = _boundaryController;
            tilesRepository = _tilesRepository;
            ratingChunksController = new RatingChunksController(_player, boundaryController, _tilesRepository);
            topTiles = new TopTiles(capacity, _player, boundaryController);
        }

        public void CheckOut(Vector2Int _position)
        {
            //удаляем только из чанков, список топов почистим позже
            ratingChunksController.CheckOutTile(_position);
        }

        public void CheckIn(IRatingTile _tile)
        {
            ratingChunksController.AddTile(_tile);
        }

        public void ShowAll()
        {
            topTiles.CheckBounds(); //чистим топ вне границ 
                                    // когда в ChunkPoints содержится достаточно чанков
            if (ratingChunksController.ChunksReady(capacity))
            {
                foreach (RatingChunk chunk in ratingChunksController.GetChunks.Values)
                {
                    // если чанк нуждается в пересчете, то выполняем
                    if (chunk.NeedRecalculate)
                    {
                        chunk.ChunkRecalculate();
                    }
                    if (chunk.BestTile != null)
                        topTiles.Add(chunk.BestTile);
                }
            }
            else
            {
                // если чанков не хватает - перебираем тайлы
                RectInt boundary = boundaryController.Boundary;
                for (int x = boundary.xMin; x <= boundary.xMax; x++)
                {
                    for (int y = boundary.yMin; y <= boundary.yMax; y++)
                    {
                        IRatingTile newTile = tilesRepository.GetTile(new Vector2Int(x, y));
                        if (newTile != null)
                            topTiles.Add(newTile);
                    }
                }
            }

            topTiles.ShowAll();
        }

        public void DestroyAll()
        {
            topTiles.DestroyAll();
        }
    }
}
