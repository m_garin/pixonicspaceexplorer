﻿using UnityEngine;
using System.Collections.Generic;
using SpaceExplorer.MathFunc;
using SpaceExplorer.Ship.Boundary;

namespace SpaceExplorer.Map.Tiles.Display.Top
{
    /// <summary>
    /// Класс TopTiles
    /// Для работы с топом 
    /// </summary>
    public class TopTiles
    {
        /// <summary>
        /// Количество ближайщих тайлов
        /// </summary>
        int capacity;
        readonly List<IRatingTile> topTiles;
        readonly IBoundaryController boundaryController;
        readonly IMathf mathf;
        readonly int playerRating;
        int minRatingDistance = int.MaxValue;

        public TopTiles(int _capacity, IRating _player, IBoundaryController _boundaryController)
        {
            capacity = _capacity;
            topTiles = new List<IRatingTile>(capacity + 1);
            mathf = new MyMathf();
            playerRating = _player.GetRating;
            boundaryController = _boundaryController;
        }

        public void Add(IRatingTile _tile)
        {
            int topTilesCount = topTiles.Count;
            bool alreadyAdded = false;
            // если список заполнен, то выполняем добавление элементов по правилам сортировки от меньшей дистанции к большей
            // т.е. на первом месте планета с меньшей дистанцией, на последнем месте с большей
            int newRatingDistance = mathf.Abs(playerRating - _tile.GetRating);

            // если дистанция до новой точки больше чем последняя граница из списка, то выходим из функции
            if (topTilesCount == capacity && newRatingDistance >= minRatingDistance)
            {
                return;
            }

            int tileX = _tile.Position.x; //чтобы в цикле не перебирать
                                          // добавляем точку в список в порядке сортировки
            for (int i = 0; i < topTilesCount; i++)
            {
                IRatingTile tile = topTiles[i];
                if (tile.Position.x == tileX && tile.Position.y == _tile.Position.y) // если точка уже содержится, то выходим
                    return;

                if (mathf.Abs(playerRating - tile.GetRating) > newRatingDistance) //сравниваем текущую дистанцию с новой
                {
                    topTiles.Insert(i, _tile);
                    minRatingDistance = mathf.Abs(playerRating - topTiles[topTiles.Count - 1].GetRating);
                    topTilesCount++;
                    alreadyAdded = true;
                    break;
                }
            }

            // если в списке больше 20-и элементов (TopPointCapacity = 20)
            // то удаляем последний элемент списка
            if (topTilesCount > capacity)
            {
                Remove(capacity);
                minRatingDistance = mathf.Abs(playerRating - topTiles[capacity - 1].GetRating);
            }
            else if (topTilesCount < capacity && !alreadyAdded)
            {
                topTiles.Add(_tile);
                minRatingDistance = mathf.Abs(playerRating - topTiles[topTiles.Count - 1].GetRating);
            }
        }

        public void DestroyAll()
        {
            foreach (IRatingTile tile in topTiles)
            {
                tile.DestroyPrefab();
            }
        }

        public void CheckBounds()
        {
            RectInt mapBoundary = boundaryController.Boundary;

            for (int i = topTiles.Count - 1; i >= 0; i--)
            {
                Vector2Int position = topTiles[i].Position;
                if (!(mapBoundary.xMin <= position.x && position.x <= mapBoundary.xMax && mapBoundary.yMin <= position.y && position.y <= mapBoundary.yMax))
                {
                    Remove(i);
                }
            }
        }

        public void ShowAll()
        {
            foreach (IRatingTile tile in topTiles)
            {
                tile.ShowPrefab();
            }
        }

        void Remove(int _index)
        {
            topTiles[_index].DestroyPrefab();
            topTiles.RemoveAt(_index);
        }
    }
}