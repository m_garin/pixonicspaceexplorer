using System.Collections.Generic;
using UnityEngine;

namespace SpaceExplorer.Map.Tiles.Display.All
{
    /// <summary>
    /// Класс AllDisplay
    /// Отображения всех тайлов в зоне видимости
    /// </summary>
    public class AllDisplay : IDisplayMode<IRatingTile>
    {
        readonly protected Dictionary<Vector2Int, IRatingTile> tiles;

        public AllDisplay()
        {
            tiles = new Dictionary<Vector2Int, IRatingTile>();
        }

        public void CheckIn(IRatingTile _tile)
        {
            tiles.Add(_tile.Position, _tile);
            _tile.ShowPrefab();
        }

        public void CheckOut(Vector2Int _position)
        {
            IRatingTile tile;
            if (tiles.TryGetValue(_position, out tile))
            {
                tile.DestroyPrefab();
                tiles.Remove(tile.Position);
            }
        }

        public void ShowAll()
        {

        }

        public void DestroyAll()
        {
            foreach (IRatingTile tile in tiles.Values)
            {
                tile.DestroyPrefab();
            }
        }
    }
}