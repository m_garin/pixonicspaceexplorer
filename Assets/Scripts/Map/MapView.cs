﻿using SpaceExplorer.CameraView;
using SpaceExplorer.Ship;
using SpaceExplorer.Ship.Boundary;
using SpaceExplorer.UI;
using SpaceExplorer.UI.DebugInfo;
using UnityEngine;

namespace SpaceExplorer.Map
{
    /// <summary>
    ///  Класс MapView
    ///  Управляет отображением карты
    /// </summary>
    sealed public class MapView : MonoBehaviour
    {

        [SerializeField]
        Player playerController;
        [SerializeField]
        CameraController cameraController;
        [SerializeField]
        MapUI mapUI;
        [SerializeField]
        MapDebugInfo mapDebug;

        IBoundaryController boundaryController;

        DrawMap drawMap;

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }

        private void Start()
        {
            boundaryController = new SquareBoundaryController(playerController);
            cameraController.SetBoundaryController = boundaryController;
            mapUI.SetBoundaryController = boundaryController;
            mapDebug.SetBoundaryController = boundaryController;

            drawMap = new DrawMap(boundaryController, playerController);
            boundaryController.ChangedEvent += drawMap.Redraw; //отрисуем, если границы видимости изменились (сместились/изменился размер)
            drawMap.Redraw(); //отрисуем первый раз
        }

        private void OnDestroy()
        {
            boundaryController.ChangedEvent -= drawMap.Redraw;
        }
    }
}