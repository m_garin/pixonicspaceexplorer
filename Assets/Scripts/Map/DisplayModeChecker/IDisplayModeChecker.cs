﻿
namespace SpaceExplorer.Map.Tiles.Display
{
    public interface IDisplayModeChecker
    {
        //метод проанализирует границы и выдаст нам подходящий способ отображения 
        IDisplayMode<IRatingTile> GetDisplayMode();

        TileGenerator GetTileGenerator();

        bool IsChanged();
    }
}