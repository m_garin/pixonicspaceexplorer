﻿using SpaceExplorer.Map.Tiles.Display.All;
using SpaceExplorer.Map.Tiles.Display.Top;
using SpaceExplorer.Map.Tiles.Repository;
using SpaceExplorer.Ship.Boundary;

namespace SpaceExplorer.Map.Tiles.Display
{
    /// <summary>
    /// Класс BaseDisplayModeChecker
    /// Проверяет границы видимости и выбирает режим отображения тайлов
    /// </summary>
    public class BaseDisplayModeChecker : IDisplayModeChecker
    {
        readonly IBoundaryController boundaryController;
        readonly IRating player;
        readonly ITilesRepository tilesRepository;
        IDisplayMode<IRatingTile> displayMode;
        readonly TileGenerator tileGenerator;
        bool isChanged = false;

        public BaseDisplayModeChecker(IBoundaryController _boundaryController, IRating _player)
        {
            boundaryController = _boundaryController;
            player = _player;
            tilesRepository = new RNDRepository(); //откуда будем брать тайлы
            displayMode = new AllDisplay();
            tileGenerator = new TileGenerator(tilesRepository, displayMode);
        }

        /// <summary>
        /// Был ли изменен режим отображения после последней проверки
        /// </summary>
        public bool IsChanged()
        {
            if (isChanged)
            {
                isChanged = false;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Выберем режим отображения, проверив границы
        /// </summary>
        public IDisplayMode<IRatingTile> GetDisplayMode()
        {
            if (boundaryController.IsSpacial())
            {
                if (!(displayMode is TopDisplay))
                {
                    isChanged = true;
                    return ChangeDisplayMode(new TopDisplay(player, boundaryController, tilesRepository));
                }
            }
            else
            {
                if (!(displayMode is AllDisplay))
                {
                    isChanged = true;
                    return ChangeDisplayMode(new AllDisplay());
                }
            }

            return displayMode;
        }

        public TileGenerator GetTileGenerator()
        {
            return tileGenerator;
        }

        IDisplayMode<IRatingTile> ChangeDisplayMode(IDisplayMode<IRatingTile> _displayMode)
        {
            displayMode.DestroyAll();
            displayMode = _displayMode;
            tileGenerator.SetTilesViewer = displayMode;
            return displayMode;
        }
    }
}