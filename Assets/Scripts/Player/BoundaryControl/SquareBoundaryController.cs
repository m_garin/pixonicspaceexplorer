using System;
using UnityEngine;

namespace SpaceExplorer.Ship.Boundary
{
    /// <summary>
    ///  Класс SquareBoundaryController
    ///  Контроль границ видимости NxN
    /// </summary>
    public class SquareBoundaryController : IBoundaryController
    {
        RectInt boundary;
        int minN = 5;
        int maxN = 10000;
        readonly MovableObject2D controlObject;
        int tilesToSpecialMode = 100;

        int MinN
        {
            get
            {
                return minN - 1;
            }
        }
        int MaxN
        {
            get
            {
                return maxN - 1;
            }
        }

        public SquareBoundaryController(MovableObject2D _controlObject)
        {
            controlObject = _controlObject;
            boundary = new RectInt(controlObject.Position, new Vector2Int(MinN, MinN));

            _controlObject.MovedObject += SetBoundaryCenter;
            SetBoundaryCenter(_controlObject.Position);
        }

        public RectInt Boundary
        {
            get
            {
                return boundary;
            }
        }

        public int GetSize
        {
            get
            {
                return (boundary.width + 1) * (boundary.height + 1);
            }
        }

        public void SetBoundaryCenter(Vector2Int _center)
        {
            boundary.position = new Vector2Int(_center.x - boundary.width / 2, _center.y - boundary.height / 2);
            ChangedEvent?.Invoke();
        }

        public void ChangeSize(int _deltaSize)
        {
            int newWidth = boundary.width + _deltaSize;
            int clampDeltaSize = Mathf.Clamp(newWidth, MinN, MaxN);
            boundary.width = clampDeltaSize;
            boundary.height = boundary.width;
            SetBoundaryCenter(controlObject.Position);
        }

        /// <summary>
        ///  Включен ли специальный режим отображения
        /// </summary>
        public bool IsSpacial()
        {
            if (GetSize > tilesToSpecialMode)
            {
                return true;
            }
            return false;
        }

        public event Action ChangedEvent;
    }
}