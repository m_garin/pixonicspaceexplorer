﻿using System;
using UnityEngine;

namespace SpaceExplorer.Ship.Boundary
{
    public interface IBoundaryController
    {
        RectInt Boundary { get; }

        void SetBoundaryCenter(Vector2Int _center);

        void ChangeSize(int _deltaSize);

        int GetSize { get; }

        bool IsSpacial();

        event Action ChangedEvent;
    }
}