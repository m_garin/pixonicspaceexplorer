﻿using UnityEngine;

namespace SpaceExplorer.Ship.Controller
{
    public interface IInputDevice2D
    {
        Vector2Int GetMove
        {
            get;
        }
    }
}