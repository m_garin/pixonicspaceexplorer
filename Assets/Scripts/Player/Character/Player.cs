﻿using SpaceExplorer.CameraView;
using SpaceExplorer.Map.Tiles;
using SpaceExplorer.Ship.Controller;
using UnityEngine;

namespace SpaceExplorer.Ship
{
    public class Player : MovableObject2D, IRating
    {
        [SerializeField]
        CameraController cameraController;
        [SerializeField]
        MobileInput mobileInput;
        IInputDevice2D inputDevice;
        int rating;

        public int GetRating
        {
            get
            {
                return rating;
            }
        }

        private void Start()
        {
#if !UNITY_EDITOR && (UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE)
            inputDevice = mobileInput;
#else
            inputDevice = new PCInput();
#endif

            //можно обычным рандомом
            rating = Random.Range(0, 10000);
            ChangeScale(cameraController.GetRelativeScale);
            cameraController.RelativeSizeChanged += ChangeScale;
        }

        void Update()
        {
            Vector2Int moveVector = inputDevice.GetMove;

            if (moveVector != Vector2Int.zero)
                Move(moveVector);
        }

        private void OnDisable()
        {
            cameraController.RelativeSizeChanged -= ChangeScale;
            ChangeScale(1);
        }
    }
}