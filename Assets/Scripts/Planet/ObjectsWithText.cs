﻿using SpaceExplorer.CameraView;
using UnityEngine;

public class ObjectsWithText : ScalableObject {

    [SerializeField]
    Sprite[] sprites;
    [SerializeField]
    SpriteRenderer spriteRenderer;
    [SerializeField]
    TextMesh textLine;
    ICamera cameraController;

    public string TextLine
    {
        set
        {
            textLine.text = value;
        }
    }

    public void ChangeSprite (int _spriteId)
    {
        spriteRenderer.sprite = sprites[_spriteId];
    }

    private void Start()
    {
        cameraController = Camera.main.GetComponent<CameraController>();
        ChangeScale(cameraController.GetRelativeScale);
        cameraController.RelativeSizeChanged += ChangeScale;
    }

    private void OnEnable()
    { 
        if (cameraController != null) 
        {
            ChangeScale(cameraController.GetRelativeScale);
            cameraController.RelativeSizeChanged += ChangeScale;
        }
    }

    private void OnDisable()
    {
        if (cameraController != null) {
            cameraController.RelativeSizeChanged -= ChangeScale;
            ChangeScale(1);
        }
    }
}
