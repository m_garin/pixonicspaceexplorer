﻿using UnityEngine;
using System;

public class ScalableObject : MonoBehaviour
{
    protected void ChangeScale(float _scale)
    {
        if (Mathf.Abs(transform.localScale.x - _scale) > Single.Epsilon)
            transform.localScale = new Vector3(_scale, _scale, transform.localScale.z);
    }
}
