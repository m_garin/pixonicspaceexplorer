﻿using System;

namespace SpaceExplorer.CameraView
{
    public interface ICamera
    {
        float GetRelativeScale
        {
            get;
        }

        event Action<float> RelativeSizeChanged;
    }
}