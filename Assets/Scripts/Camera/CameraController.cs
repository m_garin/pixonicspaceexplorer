using System;
using SpaceExplorer.Ship.Boundary;
using UnityEngine;

namespace SpaceExplorer.CameraView
{
    public class CameraController : MonoBehaviour, ICamera
    {
        Camera mainCamera;
        IBoundaryController boundaryController;
        float relativeSize = 1.0f;

        void Start()
        {
            mainCamera = Camera.main;
        }

        public event Action<float> RelativeSizeChanged;

        public float GetRelativeScale
        {
            get
            {
                if (boundaryController.IsSpacial())
                    return relativeSize;
                else
                    return 1.0f;
            }
        }

        public IBoundaryController SetBoundaryController
        {
            set
            {
                boundaryController = value;
            }
        }

        float OrthographicSize
        {
            get
            {
                return mainCamera.orthographicSize;
            }
            set
            {
                if (Math.Abs(mainCamera.orthographicSize - value) > Single.Epsilon)
                {
                    mainCamera.orthographicSize = value;
                    relativeSize = value / 10 * Screen.width / Screen.height;
                    RelativeSizeChanged?.Invoke(GetRelativeScale);
                }
            }
        }

        void LateUpdate()
        {
            if (boundaryController != null)
            {
                RectInt boundary = boundaryController.Boundary;
                float tmpZ = transform.position.z;
                Vector3 tmp = new Vector3(boundary.center.x, boundary.center.y, tmpZ);
                transform.position = tmp;
                OrthographicSize = (boundary.height + 1) / 2.0f;
            }
        }
    }
}
