using SpaceExplorer.Ship.Boundary;
using UnityEngine;
using UnityEngine.UI;

namespace SpaceExplorer.UI
{
    /// <summary>
    /// Класс MapUI
    /// Управление приближением/отдалением
    /// </summary>
    public class MapUI : MonoBehaviour
    {
        [SerializeField]
        IBoundaryController boundaryController;
        [SerializeField]
        Text multiplierTextLine;

        ushort multiplier = 1;

        public IBoundaryController SetBoundaryController
        {
            set
            {
                boundaryController = value;
            }
        }

        public float SetMultiplier
        {
            set
            {
                multiplier = (ushort)value;
                multiplierTextLine.text = 'x' + multiplier.ToString();
            }
        }

        //приблизить
        public void ZoomIn()
        {
            boundaryController.ChangeSize(-multiplier);
        }

        public void ZoomOut()
        {
            boundaryController.ChangeSize(multiplier);
        }
    }
}
