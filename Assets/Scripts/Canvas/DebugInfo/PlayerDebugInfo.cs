﻿using UnityEngine;
using UnityEngine.UI;
using SpaceExplorer.Ship;

namespace SpaceExplorer.UI.DebugInfo
{
    public class PlayerDebugInfo : MonoBehaviour
    {

        [SerializeField]
        Player player;
        [SerializeField]
        Text textLinePosition;

        private void Update()
        {
            textLinePosition.text = "Ship rating " + player.GetRating.ToString() + " pos " + player.Position.ToString();
        }
    }
}
